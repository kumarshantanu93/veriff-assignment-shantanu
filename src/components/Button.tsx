import React from "react";
import styled, { css } from 'styled-components';
import {  ButtonComponentProps } from '../types'

const ButtonComponent = styled.button<ButtonComponentProps>`
    color: #fff;
    background-color: #fff;
    color: #1e4c5d;
    padding: 5px 10px;
    min-width: 45px;
    border: none;
    font-size: 14px;
    cursor: pointer;
    ${props =>
        props.big &&
        css`
            background: #E6EBF4;
            padding: 10px 20px;
            border-radius: 5px;
            font-weight: bold;
            color: #BCC0CD;
        `};
    ${props =>
       props.selected &&
        css`
            background: #1e4c5d;
            color: #fff;
        `};
`;


const Button = ({ onClick, children, ...rest }:ButtonComponentProps) => {
    return (
        <ButtonComponent onClick={onClick} {...rest}>
            {children}
        </ButtonComponent>
    );
};

export default Button;
