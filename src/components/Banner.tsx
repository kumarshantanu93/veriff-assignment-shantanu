
import styled from 'styled-components';
import errorIcon from '../assets/error-icon.svg';
import successIcon from '../assets/success-icon.svg';
import MainContainer from '../layout/MainContainer';
import { BannerProps, IconProps } from '../types'


const BannerComponent = styled.div<BannerProps>`
    text-align: center;
`;

const IconComponent = styled.div<IconProps>`
    width: 75px;
    height: 75px;
    background: url(${(props) => (props.bannerType === 'failure' ? errorIcon : props.bannerType === 'success' ? successIcon : '')});
    background-repeat: no-repeat;
    background-size: contain;
    margin: 20px auto; 
`;

const Banner = ({ bannerType, children }: BannerProps) => {
    return (
        <MainContainer>
            <BannerComponent bannerType={bannerType}>
                <IconComponent bannerType={bannerType}></IconComponent>
                {children}
            </BannerComponent>
        </MainContainer>
    )
};

export default Banner;
