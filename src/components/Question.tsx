import React from "react";
import styled, { css } from 'styled-components';
import Checkbox from './Checkbox';

import { QuestionStyleProps, QuestionProp } from '../types'

const QuestionComponent = styled.div<QuestionStyleProps>`
    padding: 10px;
    ${props =>
        props.isQuestionDisabled &&
        css`
            opacity: 0.3;
            pointer-events: none;
    `};
    ${props => props.isQuestionActive && 'background-color: #e3f5f7'};
`;

const QuestionDescriptionComponent = styled.p`
    margin: 5px 0;
`;

const QuestionButtonContainer = styled.div`
    border: 1px solid #1e4c5d;
    border-radius: 4px;
    display: inline-block;
    overflow: hidden;
`;

const Question = ({ index, item, isQuestionDisabled, cursor, yesClick, noClick, onMouseEnter, onMouseLeave }: QuestionProp) => {
    return (
        <QuestionComponent
            isQuestionDisabled={isQuestionDisabled}
            isQuestionActive={index === cursor}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            role="question"
            >
            <QuestionDescriptionComponent tabIndex={!isQuestionDisabled ? 0 : -1}>{item.description}</QuestionDescriptionComponent>
            <QuestionButtonContainer>
                <Checkbox
                    id={`${item.id}-yes`}
                    name="yes"
                    label="yes"
                    value="yes"
                    checked={item.value === true}
                    onChange={yesClick}
                    disabled={isQuestionDisabled}
                />
                <Checkbox
                    id={`${item.id}-no`}
                    name="no"
                    label="no"
                    value="no"
                    checked={item.value === false}
                    onChange={noClick}
                    disabled={isQuestionDisabled}
                />
            </QuestionButtonContainer>
        </QuestionComponent>
    )
};

export default Question;
