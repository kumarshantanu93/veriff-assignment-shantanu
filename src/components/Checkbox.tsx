import React from "react";
import styled from "styled-components";
import { CheckBoxProp } from '../types'

const InputCheckbox = styled.input.attrs(props => ({
    id: props.id,
    name: props.name,
    value: props.value,
    disabled: props.disabled,
    checked: props.checked,
    onChange: props.onChange,
}))`
    display: none;
    &:checked + label {
        background-color: #1e4c5d;
        color: #fff;
      }
    }
`

const Label = styled.label`
  cursor: pointer;
  position: relative;
  display: inline-block;
  font-size: 14px;
  min-width: 22px;
  text-align: center;
  padding: 5px 10px;
`;

export default function Checkbox({
    id,
    name,
    value,
    disabled,
    checked,
    onChange,
    label
}: CheckBoxProp) {
    return (
        <>
            <InputCheckbox
                id={id}
                name={name}
                value={value}
                disabled={disabled}
                checked={checked}
                onChange={onChange}
                type="checkbox"
            />
            <Label
                htmlFor={id}
                role="checkbox"
                aria-checked={checked}
                tabIndex={!disabled ? 0 : -1}>
                {label}
            </Label>
        </>
    );
}
