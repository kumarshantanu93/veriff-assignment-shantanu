
import styled from 'styled-components';
import { MainContainerProps } from '../types'


const MainContainerComponent = styled.div`
      font-family: sans-serif;
      max-width: 600px;
      margin: 0 auto;
      top: 40%;
      left: 50%;
      position: absolute;
      transform: translate(-50%, -50%);
`;


const MainContainer = (props: MainContainerProps ) => {
    return (
        <MainContainerComponent>
            {props.children}
        </MainContainerComponent>
    )
};

export default MainContainer;
