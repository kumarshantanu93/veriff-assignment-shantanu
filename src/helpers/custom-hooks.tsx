import { useState, useEffect } from "react";


export const useKeyPress = (targetKey: string) => {
    const [keyPressed, setKeyPressed] = useState(false);

    const downHandler = (e: KeyboardEvent) => {
        const { key } = e;
        if (key === targetKey) {
            setKeyPressed(true);
        }
    }

    const upHandler = (e: KeyboardEvent) => {
        const { key } = e;
        if (key === targetKey) {
            setKeyPressed(false);
        }
    };

    useEffect(() => {
        window.addEventListener("keydown", downHandler);
        window.addEventListener("keyup", upHandler);
        return () => {
            window.removeEventListener("keydown", downHandler);
            window.removeEventListener("keyup", upHandler);
        };
    });

    return keyPressed;
};
