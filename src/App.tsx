import React, { useEffect, useState } from 'react';
import { useKeyPress } from './helpers/custom-hooks';
import * as api from './helpers/api'
import Banner from './components/Banner';
import MainContainer from './layout/MainContainer';
import { resType, reqType } from './types'
import Question from './components/Question';
import styled, { css } from 'styled-components';

const QuestionnaireForm = styled.form`
   width: 100%;
`;

const SubmitButtonContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    margin-top: 10px;
`;

const SubmitButton = styled.input.attrs(props => ({
  type: 'submit',
  value: 'Submit',
  disabled: props.disabled
}))`
    color: #1e4c5d;
    padding: 5px 10px;
    min-width: 45px;
    border: none;
    font-size: 14px;
    cursor: pointer;
    padding: 10px 20px;
    border-radius: 5px;
    font-weight: bold;
    color: #fff;
    background: #1e4c5d;
    ${props =>
    props.disabled &&
    css`
      background-color: #E6EBF4;
      color: #fff;
    `};
`

function App() {

  const [questionnaireState, setRes] = useState<resType[]>([]);
  const [selectedId, setSelectedId] = useState<string | null>(null);
  const [apiFailure, setApiFailure] = useState<boolean>(false);
  const [postApiSuccess, setPostApiSuccess] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [cursor, setCursor] = useState<number>(0);
  const [hovered, setHovered] = useState<string | null>(null);
  const [postData, setPostData] = useState<reqType[]>([]);
  const [isSubmitButtonDisabled, setSubmitButtonDisabled] = useState<boolean>(false);

  const downPress = useKeyPress('ArrowDown');
  const upPress = useKeyPress('ArrowUp');
  const onePress = useKeyPress('1');
  const twoPress = useKeyPress('2');

  useEffect(() => {
    setLoading(true)
    api
      .fetchChecks()
      .then((questionnaireState: resType[]) => {
        setLoading(false)
        formatResponse(questionnaireState)
      })
      .catch(err => {
        setLoading(false)
        setApiFailure(true)
      })
  }, []);

  useEffect(() => {
    if (questionnaireState.length && downPress && !checkIfPreviousQuestionIsNo(cursor + 1)) {
      setCursor(prevState =>
        prevState < questionnaireState.length - 1 ? prevState + 1 : prevState
      );
    }
  }, [downPress]);

  useEffect(() => {
    if (questionnaireState.length && upPress && !checkIfPreviousQuestionIsNo(cursor - 1)) {
      setCursor(prevState => (prevState > 0 ? prevState - 1 : prevState));
    }
  }, [upPress]);

  useEffect(() => {
    if (questionnaireState.length && hovered) {
      setCursor(questionnaireState.findIndex(item => item.id === hovered))
    }
  }, [hovered]);

  useEffect(() => {
    if (hovered || cursor) {
      setSelectedId(questionnaireState[cursor].id);
    }
  }, [hovered, cursor]);

  useEffect(() => {
    if (selectedId)
      handleAnswerSelect(selectedId, true)
  }, [onePress]);

  useEffect(() => {
    if (selectedId)
      handleAnswerSelect(selectedId, false)
  }, [twoPress]);

  useEffect(() => {
    checkSubmitButtonIsDisabled()
  }, [questionnaireState]);

  const formatResponse = (questionnaireState: resType[]) => {
    const formattedResponse = questionnaireState.sort(sortByPriority).map(addValueKeyToResponse)
    setRes(formattedResponse)
  }

  const sortByPriority = (a: resType, b: resType) => {
    return a.priority! - b.priority!
  }

  const addValueKeyToResponse = (item: resType) => {
    return { ...item, value: null }
  }

  const handleAnswerSelect = (id: string, value: boolean) => {
    let newArr = [...questionnaireState];
    let objIndex = questionnaireState.findIndex(item => item.id === id);
    newArr[objIndex].value = value
    setRes(newArr)
  }

  const checkIfPreviousQuestionIsNo = (index: number) => {
    let output = questionnaireState.slice(0, index).map(item => item.value).reduce((acc, item) => {
      return acc && item
    }, true);
    return output === null || output === false
  }

  const handleSubmitButtonClick = (e: React.FormEvent) => {
    e.preventDefault();
    const postData: reqType[] = questionnaireState
      .map(({ id, value }) => { return ({ checkId: id, value }) });
      
    api
      .submitCheckResults(postData)
      .then((questionnaireState: reqType[]) => {
        setPostData(questionnaireState)
        setPostApiSuccess(true)
      })
      .catch(() => {
        setApiFailure(true)
      })
  }

  const checkSubmitButtonIsDisabled = () => {
    const output = questionnaireState
      .map(item => item.value)
      .reduce((acc, item) => acc && item, true)

    if (output === null)
      setSubmitButtonDisabled(true)

    if (output === false || output === true)
      setSubmitButtonDisabled(false)
  }

  if (apiFailure) {
    return (
      <Banner bannerType='failure'>
        An API error occured. Please refresh the page / Try again!
      </Banner>
    )
  }

  if (loading) {
    return (
      <Banner bannerType=''>
        Loading...
      </Banner>
    )
  }

  if (postApiSuccess) {
    return (
      <>
        <Banner bannerType='success'>
          Submitted Successfully! :	&#41;
          <h4>POST DATA</h4>
          <pre style={{ textAlign: 'left' }}>
            {JSON.stringify(postData, null, 2)}
          </pre>
        </Banner>
      </>
    )
  }

  return (
    <MainContainer>
      <QuestionnaireForm onSubmit={handleSubmitButtonClick}>
        {
          questionnaireState.map((item, index) => {
            return (
              <Question
                key={item.id}
                index={index}
                item={item}
                isQuestionDisabled={checkIfPreviousQuestionIsNo(index)}
                cursor={cursor}
                yesClick={() => handleAnswerSelect(item.id, true)}
                noClick={() => handleAnswerSelect(item.id, false)}
                onMouseEnter={() => setHovered(item.id)}
                onMouseLeave={() => setHovered(item.id)}
              />
            )
          })
        }
        <SubmitButtonContainer>
          <SubmitButton disabled={isSubmitButtonDisabled} />
        </SubmitButtonContainer>
      </QuestionnaireForm>

      <a href="https://youtu.be/NxI8JFuy7BE" target="_blank" rel="noreferrer" style={{ textAlign: 'center', display: 'block', marginTop: '20px' }}>Overview Video</a>

    </MainContainer>
  );

}

export default App;