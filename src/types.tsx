export interface resType {
    id: string;
    priority?: number;
    description?: string;
    value?: boolean | null | undefined;
}

export interface reqType {
    checkId: string;
    value: boolean | null | undefined;
}

export interface MainContainerProps {
    children: React.ReactNode
}

export interface QuestionStyleProps {
    isQuestionDisabled: boolean;
    isQuestionActive: boolean;
    children: string | object;
}

export interface QuestionDescriptionProps {
    isQuestionDisabled: string;
    children: string | object;
}

export interface ButtonComponentProps {
    children: string;
    small?: boolean;
    big?: boolean;
    selected: boolean;
    disabled?: boolean;
    onClick?: (e?: React.MouseEvent) => void;
}

export interface BannerProps {
    bannerType: string;
    children: string | object;
}

export interface IconProps {
    bannerType: string;
}

export interface SubmitButtonComponentProps {
    questionnaireState: resType[];
    handleSubmitButtonClick?: (e?: React.MouseEvent) => void;
}

export interface QuestionProp {
    key: string;
    index: number;
    item: resType;
    isQuestionDisabled: boolean;
    cursor: number;
    yesClick:  () => void;
    noClick: () => void;
    onMouseEnter: () => void;
    onMouseLeave:  () => void;
}

export interface CheckBoxProp {
    id: string;
    name?: string;
    value: string;
    disabled: boolean; 
    checked: boolean;
    onChange:  () => void;
    label: string;
}