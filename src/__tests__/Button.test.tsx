import { render, screen, cleanup } from '@testing-library/react'
import Button from '../components/Button';

test('should have background color #1e4c5d when selected', () => {
    render(<Button
        selected={true}
        disabled={false}
    >yes</Button>)
    const button = screen.getByText('yes');
    expect(button).toHaveStyle(`background: #1e4c5d;`)
})

test('should be disabled when disabled prop is true', () => {
    render(<Button
        selected={false}
        disabled={true}
    >no</Button>)
    const button = screen.getByText('no');
    expect(button).toHaveAttribute('disabled')
})


