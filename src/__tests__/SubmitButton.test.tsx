import { render, screen, cleanup } from '@testing-library/react'
import SubmitButton from '../components/SubmitButton';
import { resType } from '../types'


const questionnaireStateOne: resType[] = [
    {
        "id": "ddd",
        "priority": 3,
        "description": "Document data is clearly visible",
        "value": null
    },
    {
        "id": "bbb",
        "priority": 5,
        "description": "Veriff supports presented document",
        "value": null
    },
    {
        "id": "ccc",
        "priority": 7,
        "description": "Face is clearly visible",
        "value": null
    },
    {
        "id": "aaa",
        "priority": 10,
        "description": "Face on the picture matches face on the document",
        "value": null
    }
]

test('should be disabled when all buttons are unselected', () => {
    render(<SubmitButton questionnaireState={questionnaireStateOne} />)

    const submitButtonComponent = screen.getByText('Submit');
    expect(submitButtonComponent).toHaveAttribute('disabled')
})


const questionnaireStateTwo: resType[] = [
    {
        "id": "ddd",
        "priority": 3,
        "description": "Document data is clearly visible",
        "value": true
    },
    {
        "id": "bbb",
        "priority": 5,
        "description": "Veriff supports presented document",
        "value": true
    },
    {
        "id": "ccc",
        "priority": 7,
        "description": "Face is clearly visible",
        "value": false
    },
    {
        "id": "aaa",
        "priority": 10,
        "description": "Face on the picture matches face on the document",
        "value": null
    }
]

test('should not be diabled when atleast one question is a "No"', () => {
    render(<SubmitButton questionnaireState={questionnaireStateTwo} />)

    const submitButtonComponent = screen.getByText('Submit');
    expect(submitButtonComponent).toBeEnabled() 
})


const questionnaireStateThree: resType[] = [
    {
        "id": "ddd",
        "priority": 3,
        "description": "Document data is clearly visible",
        "value": true
    },
    {
        "id": "bbb",
        "priority": 5,
        "description": "Veriff supports presented document",
        "value": true
    },
    {
        "id": "ccc",
        "priority": 7,
        "description": "Face is clearly visible",
        "value": true
    },
    {
        "id": "aaa",
        "priority": 10,
        "description": "Face on the picture matches face on the document",
        "value": null
    }
]

test('should be disabled when atleast none of the buttons is "No" and not every button is "yes"', () => {
    render(<SubmitButton questionnaireState={questionnaireStateThree} />)

    const submitButtonComponent = screen.getByText('Submit');
    expect(submitButtonComponent).toHaveAttribute('disabled')
})